//
//  ViewController.swift
//  StockIt
//
//  Created by Riccardo Crippa on 10/2/15.
//  Copyright © 2015 Riccardo Crippa. All rights reserved.
//
// http://stackoverflow.com/questions/31254725/transport-security-has-blocked-a-cleartext-http
// important website

import UIKit

class ViewController: UIViewController {
    
    var stock:Stock!
    
    @IBOutlet weak var stockTextfield: UITextField!
    @IBOutlet weak var stockNameLabel: UILabel!
    @IBOutlet weak var stockSymbolLabel: UILabel!
    @IBOutlet weak var stockPriceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.stockNameLabel.text = ""
        self.stockPriceLabel.text = ""
        self.stockSymbolLabel.text = ""
        self.title = "Stock It"
        self.stock = Stock()
        
    }
    
    @IBAction func stockIt(sender: AnyObject) {
        self.stockTextfield.resignFirstResponder()
        self.stockNameLabel.text = "Loading..."
        self.stockPriceLabel.text = ""
        self.stockSymbolLabel.text = ""
        let ss = StockService()
        ss.getStock(self.stockTextfield.text!) { (response) -> () in
            self.stock = StockService.extractStock(response)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.stockNameLabel.text = "\(self.stock.name)";
                self.stockPriceLabel.text = "\(self.stock.price)"
                self.stockSymbolLabel.text = "\(self.stock.symbol)"
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

