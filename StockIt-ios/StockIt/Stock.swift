//
//  Stock.swift
//  Stockit
//
//  Created by Riccardo Crippa on 10/2/15.
//  Copyright © 2015 Riccardo Crippa. All rights reserved.
//

import Foundation

class Stock {
    
    var symbol:String = ""
    var name:String = ""
    var price:String = ""
    
}