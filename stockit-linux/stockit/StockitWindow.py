# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# This file is in the public domain
### END LICENSE

import gettext
import urllib
import json
from gettext import gettext as _
gettext.textdomain('stockit')

from gi.repository import Gtk # pylint: disable=E0611
import logging
logger = logging.getLogger('stockit')

def function(symbol):
     r = urllib.urlopen("http://itjustworks.it/stockit/server.php?symbol="+symbol).read()
     a = json.loads(r)
     return a

from stockit_lib import Window
from stockit.AboutStockitDialog import AboutStockitDialog
from stockit.PreferencesStockitDialog import PreferencesStockitDialog

# See stockit_lib.Window.py for more details about how this class works
class StockitWindow(Window):
    __gtype_name__ = "StockitWindow"

    def finish_initializing(self, builder): # pylint: disable=E1002
        """Set up the main window"""
        super(StockitWindow, self).finish_initializing(builder)

        self.AboutDialog = AboutStockitDialog
        self.PreferencesDialog = PreferencesStockitDialog

        # Code for other initialization actions should be added here.
        self.entry1 = self.builder.get_object("entry1")
        self.label1 = self.builder.get_object("label1")
        self.label2 = self.builder.get_object("label2")
        self.label3 = self.builder.get_object("label3")
        self.label1.set_text("")
		self.label2.set_text("")
		self.label3.set_text("")

	def on_button1_clicked(self, Widget):
		symbol = self.entry1.get_text()
		response = function(symbol)
		self.label1.set_text(response["name"])
		self.label2.set_text(response["symbol"])
		self.label3.set_text(response["price"])
