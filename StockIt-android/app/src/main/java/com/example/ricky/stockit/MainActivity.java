package com.example.ricky.stockit;

import android.content.Context;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends ActionBarActivity {

    TextView nameLabel;
    TextView priceLabel;
    TextView symbolLabel;

    EditText textfield;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nameLabel = (TextView) findViewById(R.id.nameLabel);
        priceLabel = (TextView) findViewById(R.id.priceLabel);
        symbolLabel = (TextView) findViewById(R.id.symbolLabel);
        textfield = (EditText) findViewById(R.id.textfield);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // uncomment this two lines if you do not have a AsyncTask extended class.
        //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        //StrictMode.setThreadPolicy(policy);

        return true;
    }

    public void sendMessage(View v)
    {
        View view = this.getCurrentFocus();
        if(view != null)
        {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        String symbol = textfield.getText().toString();
        StockSearchAsync stockSearchAsync = new StockSearchAsync();
        stockSearchAsync.execute(symbol);

    }

    private class StockSearchAsync extends AsyncTask<String, Void, Stock>{


        @Override
        protected Stock doInBackground(String... params) {
            StockService stockService = new StockService();
            return stockService.getStock(params[0]);
        }

        // direct access to the UI thread.
        @Override
        protected void onPreExecute() {
            nameLabel.setText("Loading...");
            priceLabel.setText("");
            symbolLabel.setText("");
        }

        // direct access to the UI thread.
        @Override
        protected void onPostExecute(Stock stock) {
            nameLabel.setText(stock.name);
            symbolLabel.setText(stock.symbol);
            priceLabel.setText(stock.price);
        }
    }

}
