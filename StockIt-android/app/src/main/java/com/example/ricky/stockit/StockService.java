package com.example.ricky.stockit;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Ricky on 10/2/15.
 */

public class StockService {

    private String URL = "http://itjustworks.it/stockit/server.php";
    private String NAME = "name";
    private String SYMBOL = "symbol";
    private String PRICE = "price";

    public Stock getStock(String symbol)
    {
        String url = this.URL + "?symbol=" + symbol;
        Stock stock = new Stock();
        try {
            // It's in JSON
            String result = makeRequest(url);
            JSONObject root = new JSONObject(result);

            stock.name = root.getString(this.NAME);
            stock.price = root.getString(this.PRICE);
            stock.symbol = root.getString(this.SYMBOL);
        } catch (JSONException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return stock;
    }

    private String makeRequest(String uri) throws ClientProtocolException, IOException {
        // declare our return variable.
        String result = "";

        // make a get call to HTTP.
        HttpGet httpGet = new HttpGet(uri);

        // handle the response that we get in return
        ResponseHandler<String> responseHandler = new BasicResponseHandler();

        // create an HTTPClient, which will coordiate the get and response.
        HttpClient httpClient = new DefaultHttpClient();

        // send the URI to the get method, and have the response handler parse it and return a
        // result to us.
        result = httpClient.execute(httpGet, responseHandler);

        // return our return variable. It's in JSON format
        return result;
    }

}
