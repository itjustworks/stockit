function quote()
{
	$('#nameSpan').html("Loading...");
	$('#symbolSpan').html("");
	$('#priceSpan').html("");
	$('#dateSpan').html("");
	$('#timeSpan').html("");
	$('#changeSpan').html("");
	$('#change_in_percentSpan').html("");
	var symbol = $('#symbol').val();
	if(symbol != '')
	{
		var url = 'server.php?symbol=' + $('#symbol').val();
		$.getJSON(url, function(data) {
			$("#symbol").val("");
			$('#nameSpan').html(data.name);
			$('#symbolSpan').html(data.symbol);
			$('#priceSpan').html(data.price);
			$('#dateSpan').html(data.date);
			$('#timeSpan').html(data.time);
			$('#changeSpan').html(data.change);
			$('#change_in_percentSpan').html(data.change_in_percent);
		});
	} else {
		$('#nameSpan').html('');
		$('#symbolSpan').html("Please enter a symbol name");
	}
}
