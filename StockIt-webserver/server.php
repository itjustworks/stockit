<?php

		if(isset($_GET["symbol"]) && !empty($_GET["symbol"])) 
		{
			$symbol = $_GET["symbol"];
			// first type file: sl1d1t1c1ohgv
			// second type file: snl1d1t1c1p2 -- I use this here!!!!!
			$csv = fopen("http://download.finance.yahoo.com/d/quotes.csv?s={$symbol}&f=snl1d1t1c1p2&e=.csv", "r");
			if($csv != NULL)
			{
				$data = fgetcsv($csv);
				if($data != NULL)
				{
					if(floatval($data[2]) === 0.00)
					{
						$stock = array(
							"symbol" => "Symbol not found", 
							"name" => "", 
							"price" => "", 
							"date" => "",
							"time" => "",
							"change" => "",
							"change_in_percent" => ""
							);
						echo json_encode($stock);
					}
					else 
					{
						$stock = array(
							"symbol" => $data[0], 
							"name" => $data[1], 
							"price" => $data[2],
							"date" => $data[3],
							"time" => $data[4],
							"change" => $data[5],
							"change_in_percent" => $data[6]
						);
						echo json_encode($stock);
					}
					//echo json_encode($data);
				}
				fclose($csv);		
			}
		}

?>
