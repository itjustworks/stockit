﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockIt_windowsForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Text = "";
            label2.Text = "";
            label3.Text = "";
        }

        private dynamic function(String symbol)
        {
            String HostURI = "http://itjustworks.it/stockit/server.php?symbol=" + symbol;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(HostURI);
            request.Method = "GET";
            String test = String.Empty;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                test = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
            }
            dynamic stuff = JObject.Parse(test);
            return stuff;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String symbol = textBox1.Text;
            label1.Text = "Loading...";
            label2.Text = "";
            label3.Text = "";
            dynamic stuff = function(symbol);
            label1.Text = stuff.name;
            label2.Text = stuff.symbol;
            label3.Text = stuff.price;
        }
    }
}
