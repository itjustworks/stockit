//
//  ViewController.swift
//  StockIt-appletv
//
//  Created by Riccardo Crippa on 12/27/15.
//  Copyright © 2015 Riccardo Crippa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var stock:Stock!
    @IBOutlet weak var stockTextField: UITextField!
    @IBOutlet weak var stockNameLabel: UILabel!
    @IBOutlet weak var stockSymbolLabel: UILabel!
    @IBOutlet weak var stockPriceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "StockIt"
        self.stockNameLabel.text = ""
        self.stockPriceLabel.text = ""
        self.stockSymbolLabel.text = ""
    }
    
    @IBAction func stockItButtonPressed(sender: AnyObject) {
        self.stockNameLabel.text = "Loading..."
        self.stockPriceLabel.text = ""
        self.stockSymbolLabel.text = ""
        let ss = StockService()
        ss.getStock(self.stockTextField.text!) { (response) -> () in
            self.stock = StockService.extractStock(response)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.stockNameLabel.text = "\(self.stock.name)"
                self.stockPriceLabel.text = "\(self.stock.price)"
                self.stockSymbolLabel.text = "\(self.stock.symbol)"
            })
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

