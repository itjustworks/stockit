# StockIt

Questo è un progetto open-source che dimostra come creare un semplice servizio per tutte le piattaforme.
La parte web si trova sul sito itjustworks mentre le app non sono sugli store ma disponibili solo open source in questo repo

Trovate il sito [qua](http://itjustworks.it/stockit)
