//
//  AppDelegate.swift
//  StockIt-mac
//
//  Created by Riccardo Crippa on 12/27/15.
//  Copyright © 2015 Riccardo Crippa. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

