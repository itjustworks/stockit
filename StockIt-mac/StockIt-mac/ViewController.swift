//
//  ViewController.swift
//  StockIt-mac
//
//  Created by Riccardo Crippa on 12/27/15.
//  Copyright © 2015 Riccardo Crippa. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    var stock:Stock!

    @IBOutlet weak var stockTextField: NSTextField!
    @IBOutlet weak var stockNameLabel: NSTextField!
    @IBOutlet weak var stockSymbolLabel: NSTextField!
    @IBOutlet weak var stockPriceLabel: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.stockNameLabel.stringValue = ""
        self.stockPriceLabel.stringValue = ""
        self.stockSymbolLabel.stringValue = ""
    }
    
    @IBAction func stockItButtonPressed(sender: AnyObject) {
        self.stockNameLabel.stringValue = "Loading..."
        self.stockPriceLabel.stringValue = ""
        self.stockSymbolLabel.stringValue = ""
        let ss = StockService()
        ss.getStock(self.stockTextField.stringValue) { (response) -> () in
            self.stock = StockService.extractStock(response)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.stockNameLabel.stringValue = "\(self.stock.name)"
                self.stockPriceLabel.stringValue = "\(self.stock.price)"
                self.stockSymbolLabel.stringValue = "\(self.stock.symbol)"
            })
        }
    }
    

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

