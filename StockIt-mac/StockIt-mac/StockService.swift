//
//  StockService.swift
//  Stockit
//
//  Created by Riccardo Crippa on 10/2/15.
//  Copyright © 2015 Riccardo Crippa. All rights reserved.
//

import Foundation

private struct StockIt {
    static let URL = "http://itjustworks.it/stockit/server.php"
    struct Stock {
        static let name = "name"
        static let symbol = "symbol"
        static let price = "price"
    }
}

class StockService {


    func getStock(symbol:String, handler:(NSDictionary) -> ()){

        let url = "\(StockIt.URL)?symbol=\(symbol)"
        request(url, handler: handler)

    }


    func request(url:String, handler:(NSDictionary) -> (Void)){
        let nsUrl = NSURL(string: url)

        let task = NSURLSession.sharedSession().dataTaskWithURL(nsUrl!, completionHandler: { (data, response, error) -> Void in
            do{
                if let response = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary {
                    handler(response)
                }
            } catch _ as NSError {

            }
        })
        task.resume()
    }

    static func extractStock(response:NSDictionary) -> Stock{
        let stockInt = Stock()
        stockInt.name = response[StockIt.Stock.name] as! String
        stockInt.price = response[StockIt.Stock.price] as! String
        stockInt.symbol = response[StockIt.Stock.symbol] as! String
        return stockInt
    }

}
