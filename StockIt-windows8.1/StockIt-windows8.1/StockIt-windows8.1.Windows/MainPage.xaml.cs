﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Newtonsoft.Json;
using System.Net.Http;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento per la pagina vuota è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkId=234238

namespace StockIt_windows8._1
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            textBox.Text = "";
            textBox.PlaceholderText = "Enter here a symbol...";
            namelabel.Text = "";
            symbollabel.Text = "";
            pricelabel.Text = "";
        }

        private void stockit_Click(object sender, RoutedEventArgs e)
        {
            namelabel.Text = "Loading...";
            symbollabel.Text = "";
            pricelabel.Text = "";
            this.getStockAsync(textBox.Text, namelabel, symbollabel, pricelabel);
        }

        private async void getStockAsync(string symbol, TextBlock textblock, TextBlock textblock1, TextBlock textblock2)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = await client.GetAsync(StockService.URL + symbol))
                {
                    using (HttpContent content = response.Content)
                    {
                        string mycontent = await content.ReadAsStringAsync();
                        Stock s = JsonConvert.DeserializeObject<Stock>(mycontent);
                        textblock.Text = s.name;
                        textblock1.Text = s.symbol;
                        textblock2.Text = s.price;
                    }
                }
            }
        }
    }
}
